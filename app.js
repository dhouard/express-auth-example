var express         = require('express'),
    mongoose        = require('mongoose'),
    passport        = require('passport'),
    bodyParser      = require('body-parser'),
    localStrategy   = require('passport-local'),
    localMongoose   = require('passport-local-mongoose');

var User            = require('./model/user');

mongoose.connect('mongodb://0.0.0.0:27017/auth_app', {useNewUrlParser: true});
var app = express();

app.use(express.static('/public'));
app.use(bodyParser.urlencoded({extended: true}));

app.use(require('express-session')({
    secret: 'Una frase que yo me invente',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
passport.use(new localStrategy(User.authenticate()));

app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    res.render('home');
});

app.get('/home', function(req, res) {
    res.redirect('/');
});

app.get('/secret', isLoggedIn, function(req, res) {
    res.render('secret');
});

app.get('/register', function(req, res) {
    res.render('register');
});

app.get('/login', function(req, res) {
    res.render('login');
});

app.post('/login', passport.authenticate('local', {
    successRedirect: '/secret',
    failureRedirect: '/login'
}),function(req, res) {

});

app.post('/register', function(req, res) {
    User.register(new User({username: req.body.username}), req.body.password, function(err, user) {
        if (err) {
            return res.render('register');
        }

        passport.authenticate('local')(req, res, function() {
            res.redirect('/secret');
        })
    });
});

app.get('/logout', function(req, res){
    req.logout();
    res.redirect('/home');
});

app.listen(3000, function() {
    console.log('Server started.');
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/login');
    }
}