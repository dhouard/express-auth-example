var mongoose        = require('mongoose'),
    localMongoose   = require('passport-local-mongoose');

var userSchema  = mongoose.Schema({
    username: String,
    password: String
});

userSchema.plugin(localMongoose);

module.exports = mongoose.model('User', userSchema);